import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import http from "./services/httpService";
import { FileUpload } from "./common/fileUpload";

// extend the rendering input and validate function from Form component
class Updatepinform extends Form {
  // set state according to the original data
  state = {
    _id: this.props.originalId,
    data: [],
    dataHeader: {
      name: this.props.originalName,
      lat: this.props.originalLat,
      long: this.props.originalLong,
      fileList: this.props.originalfileList,
    },
    errors: {},
  };
  // define schema for validate input data
  schema = {
    name: Joi.string().required().label("Station name"),
    lat: Joi.number().min(-90).max(90).required().label("Latitude"),
    long: Joi.number().min(-180).max(180).required().label("Longtitude"),
    fileList: Joi.required(),
  };
  // function for upload csv file
  handleUpload = async (file) => {
    const dataHeader = {
      ...this.state.dataHeader,
    };
    console.log(dataHeader);
    this.setState({ dataHeader });

    const formData = new FormData();
    formData.append("file", file);
    //upload data to backend
    try {
      const res = await http.post("http://localhost:5000/data/csv", formData, {
        headers: {
          "Context-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      }
    }
  };
  //function for put the data(update existing data )
  doSubmit = () => {
    //submit edit data to database
    console.log("Submitted");
    const payload = {
      ...this.state.dataHeader,
      _id: this.state._id,
    };
    console.log(payload);
    this.props.handleUpdate(payload);
    this.props.handleClose();
  };

  render() {
    console.log(this.props);
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("name", "Station name")}
          {this.renderInput("lat", "Latitude", "Latitude")}
          {this.renderInput("long", "Longtitude", "Longtitude")}
          {this.renderButton("Update")}
        </form>
      </div>
    );
  }
}

export default Updatepinform;
