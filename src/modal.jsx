import React, { Component, useState } from "react";
import { Modal, Button } from "react-bootstrap";
import Addpinform from "./addPinForm";
import Updatepinform from "./updatePinForm";
import Addfileform from "./addFileform";
import Editdataform from "./editDataform";

//modal component for rendering modal and modal
function Modalwindow(props) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const renderInputField = () => {
    switch (props.action) {
      case "addStation":
        return (
          <React.Fragment>
            <Button
              style={{ marginBottom: "10px" }}
              variant="primary"
              onClick={handleShow}
            >
              Add Station
            </Button>
            <Modal show={show} onHide={handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Add Station Information</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Addpinform
                  handleAdd={props.handleAdd}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "editStation":
        return (
          <React.Fragment>
            <Button variant="btn btn-success btn-sm" onClick={handleShow}>
              Edit
            </Button>
            <Modal show={show} onHide={handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Edit Station Information</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Updatepinform
                  originalName={props.originalName}
                  originalLat={props.originalLat}
                  originalLong={props.originalLong}
                  originalfileList={props.originalfileList}
                  originalId={props.originalId}
                  handleUpdate={props.handleUpdate}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "addData":
        return (
          <React.Fragment>
            <Button variant="btn btn-primary btn-sm mr-3" onClick={handleShow}>
              Add Data
            </Button>
            <Modal show={show} onHide={handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Add Data to Station</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Addfileform
                  originalName={props.originalName}
                  originalLat={props.originalLat}
                  originalLong={props.originalLong}
                  originalfileList={props.originalfileList}
                  originalId={props.originalId}
                  handleUpdate={props.handleUpdate}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "editData":
        return (
          <React.Fragment>
            <Button variant="btn btn-primary btn-sm mr-3" onClick={handleShow}>
              Edit Data
            </Button>
            <Modal show={show} onHide={handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Add Data to Station</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Editdataform
                  originalFilename={props.originalFilename}
                  originalmainType={props.originalmainType}
                  originalsubType={props.originalsubType}
                  originalvisualFormat={props.originalvisualFormat}
                  originalName={props.originalName}
                  originalLat={props.originalLat}
                  originalLong={props.originalLong}
                  originalfileList={props.originalfileList}
                  originalId={props.originalId}
                  handleUpdate={props.handleUpdate}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
    }
  };
  return (
    <div>
      {/* conditional button rendering for add and update station modal */}
      {renderInputField()}
    </div>
  );
}

export default Modalwindow;
