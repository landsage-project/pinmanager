import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import http from "./services/httpService";
import { FileUpload } from "./common/fileUpload";
import Progress from "./common/progress";
import { mainType, subType, visualFormat } from "./DataTypeSelector.js";
//component for render form inside modal
class Addpinform extends Form {
  state = {
    data: [],
    fileType:"",
    dataHeader: {
      name: "",
      lat: "",
      long: "",
      maintype: "",
      subtype: "",
      visualformat: "",
      fileName: "",
    },
    errors: {},
    uploadPercentage: 0,
    existData: [],
  };
  //define data schema
  schema = {
    name: Joi.string().required().label("Station name"),
    lat: Joi.number().min(-90).max(90).required().label("Latitude"),
    long: Joi.number().min(-180).max(180).required().label("Longtitude"),
    maintype: Joi.string().required().label("Main DataType"),
    subtype: Joi.string().required().label("Sub DataType"),
    visualformat: Joi.string().required().label("Visualize Format"),
    fileName: Joi.required(),
  };
  // function for posting data(add station)
  handleUpload = async (file) => {
    const dataHeader = {
      ...this.state.dataHeader,
      fileName: file.name,
    };
    this.setState({ dataHeader });
    //this.setState({ uploadPercentage: 0 });
    const formData = new FormData();
    formData.append("file", file);
    //upload csv file to backend
    try {
      const res = await http.post("http://localhost:5000/data/", formData, {
        headers: {
          "Context-Type": "multipart/form-data",
        },
        onUploadProgress: (progressEvent) => {
          //update progress bar state
          const uploadPercentage =
            (progressEvent.loaded / progressEvent.total) * 100;

          this.setState({ uploadPercentage });
          setTimeout(() => this.setState({ uploadPercentage: 0 }), 1000);
        },
      });
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      }
    }
  };
  doSubmit = () => {
    let filetype = ""
    switch (true) {
      case this.state.dataHeader.fileName.includes(".csv") || this.state.dataHeader.fileName.includes(".CSV"):
        filetype = "csv";break;
      case this.state.dataHeader.fileName.includes(".png") || this.state.dataHeader.fileName.includes(".PNG"):
        filetype = "texture";break;
      default : filetype = "csv";
    
    }
    const payload = {
      name: this.state.dataHeader.name,
      lat: this.state.dataHeader.lat,
      long: this.state.dataHeader.long,
      fileList: [
        {
          mainType: this.state.dataHeader.maintype,
          subType: this.state.dataHeader.subtype,
          visualFormat: this.state.dataHeader.visualformat,
          fileType: filetype,
          fileName: this.state.dataHeader.fileName,
        },
      ],
    };
    //upload data to the database
    console.log(payload);
    this.props.handleAdd(payload);
    this.props.handleClose();
  };
  rendersubSelect() {
    console.log(this.state.dataHeader.fileName);
    switch (this.state.dataHeader.maintype) {
      case "Flooding":
        return this.renderSelect("subtype", "Sub DataType", subType.flooding);
      case "Landslide":
        return this.renderSelect("subtype", "Sub DataType", subType.landslide);
    }
  }
  renderVisualtype(fileName) {
    console.log(fileName.includes(".png"));
    switch (true) {
      case fileName.includes(".csv") || fileName.includes(".CSV"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          visualFormat.timeSeries
        );
      case fileName.includes(".png") || fileName.includes(".PNG"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          visualFormat.textureMap
        );
    }
  }
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput(
            "name",
            "Station name",
            "Station name",
            "Enter Station Name"
          )}
          {this.renderInput("lat", "Latitude", "Latitude", "Enter latitude")}
          {this.renderInput(
            "long",
            "Longtitude",
            "Longtitude",
            "Enter Longtitude"
          )}
          {this.renderSelect("maintype", "Main DataType", mainType)}
          {this.rendersubSelect()}

          {/* conditional rendering progress bar */}
          <FileUpload handleUpload={this.handleUpload} />
          {this.state.uploadPercentage === 0 ? null : (
            <Progress percentage={this.state.uploadPercentage} />
          )}
          {this.renderVisualtype(this.state.dataHeader.fileName)}

          <button
            style={{ margin: 10 }}
            disabled={
              this.validate() !== null || this.state.dataHeader.fileName === ""
            }
            className="btn btn-primary float-right"
          >
            {console.log(
              this.validate() !== null || this.state.dataHeader.fileName === ""
            )}
            Confirm
          </button>
        </form>
      </div>
    );
  }
}

export default Addpinform;
