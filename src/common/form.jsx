import React, { Component } from "react";
import Joi from "joi-browser";
import Input from "./input";
import Select from "./select";
//class for rendering input form and validate data intregity
class Form extends Component {
  state = {
    dataHeader: {},
    errors: {},
  };
  validate = () => {
    const options = { abortEarly: false };
    const { error } = Joi.validate(this.state.dataHeader, this.schema, options);
    if (!error) return null;
    const errors = {};
    for (let item of error.details) errors[item.path[0]] = item.message;
    return errors;
  };

  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const errors = this.validate();
    this.setState({ errors: errors || {} });
    if (errors) return;

    //call the server
    this.doSubmit();
  };
  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    const dataHeader = { ...this.state.dataHeader };
    dataHeader[input.name] = input.value;
    this.setState({ dataHeader, errors });
  };
  renderButton(label) {
    return (
      <button
        style={{ margin: 10 }}
        disabled={this.validate()}
        className="btn btn-primary float-right"
      >
        {label}
      </button>
    );
  }
  renderSelect(name, label, options) {
    const { dataHeader, errors } = this.state;
    return (
      <Select
        name={name}
        value={dataHeader[name]}
        label={label}
        options={options}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  }
  renderInput(name, label, type = "text", placeholder) {
    const { dataHeader, errors } = this.state;
    return (
      <Input
        placeholder={placeholder}
        type={type}
        name={name}
        value={dataHeader[name]}
        label={label}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  }
}

export default Form;
