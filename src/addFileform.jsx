import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import http from "./services/httpService";
import { FileUpload } from "./common/fileUpload";
import { mainType, subType, visualFormat } from "./DataTypeSelector.js";

// extend the rendering input and validate function from Form component
class Addfileform extends Form {
  // set state according to the original data
  state = {
    _id: this.props.originalId,
    data: [],
    dataHeader: {
      maintype: "",
      subtype: "",
      fileName: "",
      visualformat: "",
    },
    errors: {},
  };
  // define schema for validate input data
  schema = {
    maintype: Joi.string().required().label("Main DataType"),
    subtype: Joi.string().required().label("Sub Data Type"),
    visualformat: Joi.string().required().label("Visualize Format"),
    fileName: Joi.required(),
  };
  // function for upload csv file
  handleUpload = async (file) => {
    const dataHeader = { ...this.state.dataHeader, fileName: file.name };
    console.log(dataHeader);
    this.setState({ dataHeader });

    const formData = new FormData();
    formData.append("file", file);
    //upload data to backend
    try {
      const res = await http.post("http://localhost:5000/data/", formData, {
        headers: {
          "Context-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      }
    }
  };
  //function for put the data(update existing data )
  doSubmit = () => {
    //submit edit data to database
    console.log("Submitted");
    let filetype = ""
    switch (true) {
      case this.state.dataHeader.fileName.includes(".csv") || this.state.dataHeader.fileName.includes(".CSV"):
        filetype = "csv";break;
      case this.state.dataHeader.fileName.includes(".png") || this.state.dataHeader.fileName.includes(".PNG"):
        filetype = "texture";break;
      default : filetype = "csv";
    
    }
    const FL = [...this.props.originalfileList];
    if (FL.some((file) => file.subType === this.state.dataHeader.subtype)) {
      FL.map((file) =>
        file.subType === this.state.dataHeader.subtype
          ? (file.fileName = this.state.dataHeader.fileName)
          : null
      );
    } else {
      FL.push({
        mainType: this.state.dataHeader.maintype,
        subType: this.state.dataHeader.subtype,
        visualFormat: this.state.dataHeader.visualformat,
        fileType: filetype,
        fileName: this.state.dataHeader.fileName,
      });
    }

    console.log(FL);
    const payload = {
      name: this.props.originalName,
      lat: this.props.originalLat,
      long: this.props.originalLong,
      _id: this.state._id,
      fileList: [...FL],
    };
    console.log(payload);
    this.props.handleUpdate(payload);
    this.props.handleClose();
  };
  rendersubSelect() {
    console.log(this.state.dataHeader.fileName);
    switch (this.state.dataHeader.maintype) {
      case "Flooding":
        return this.renderSelect("subtype", "Sub DataType", subType.flooding);
      case "Landslide":
        return this.renderSelect("subtype", "Sub DataType", subType.landslide);
    }
  }
  renderVisualtype(fileName) {
    console.log(fileName.includes(".png"));
    switch (true) {
      case fileName.includes(".csv"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          visualFormat.timeSeries
        );
      case fileName.includes(".png"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          visualFormat.textureMap
        );
    }
  }

  render() {
    console.log(this.props);
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {this.renderSelect("maintype", "Main DataType", mainType)}
          {this.rendersubSelect()}

          <FileUpload
            originalfileName={this.state.dataHeader.fileName}
            handleUpload={this.handleUpload}
          />
          {this.renderVisualtype(this.state.dataHeader.fileName)}

          {this.renderButton("Update")}
        </form>
      </div>
    );
  }
}

export default Addfileform;
