import React, { Component } from "react";
import { ToastContainer } from "react-toastify";
import http from "./services/httpService";
import config from "./config.json";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import Modalwindow from "./modal";
import Badges from "./common/badge";
import BootstrapTable from "react-bootstrap-table-next";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import { Tab } from "bootstrap";
import { withStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { render } from "react-dom";
import TableCell from "@material-ui/core/TableCell";
const StyledCell = withStyles({
  head: {
    fontWeight: 700,
    fontSize: 20,
  },
  root: {
    backgroundColor: "white",
  },
})(TableCell);

class App extends Component {
  state = {
    open: false,
    posts: [],
    modalState: false,
  };
  //call data from backend
  async componentDidMount() {
    const { data: posts } = await http.get(config.apiEndpoint);
    console.log(posts);
    this.setState({ posts });
  }
  //functon for open and close modal
  handleModalState = (modalState) => {
    modalState = !modalState;
    this.setState({ modalState });
  };
  //fucntion for post payload(new new station) to backend
  handleAdd = async (obj) => {
    const { data: post } = await http.post(config.apiEndpoint, obj);
    console.log(post);
    const posts = [post, ...this.state.posts];
    this.setState({ posts });
  };
  //fucntion for put payload(edit existing  station) to backend
  handleUpdate = async (post) => {
    post.title = "UPDATED";
    console.log(post);

    const result = await http.put(config.apiEndpoint + "/" + post._id, post);
    console.log(result);
    const posts = [...this.state.posts];
    const index = posts.map((post) => post._id).indexOf(post._id);
    posts[index] = { ...post };
    console.log(posts);
    this.setState({ posts });
  };
  //fucntion for delete existing data
  handleDelete = async (post) => {
    const originalPosts = this.state.posts;

    const posts = this.state.posts.filter((p) => p._id !== post._id);
    this.setState({ posts });
    try {
      await http.delete(config.apiEndpoint + "/" + post._id);
    } catch (ex) {
      //expected(404 not found, 400 bad request)
      // display a specific error message
      console.log(ex.response);
      if (ex.response && ex.response.status === 404) {
        alert("This post has already been deleted");
        console.log("error:", ex);
      }

      //Unexpected (network down , server down, db down , bug)
      // Display a generic and friendly error message

      this.setState({ posts: originalPosts });
    }
  };
  handleDelData = (row, file) => {
    console.log(file.subType);
    const FL = [...row.fileList];
    const newFL = FL.filter(
      (payloadfile) => payloadfile.subType !== file.subType
    );
    console.log(FL);
    const payload = {
      name: row.name,
      lat: row.lat,
      long: row.long,
      _id: row._id,
      fileList: [...newFL],
    };
    console.log(payload);
    this.handleUpdate(payload);
  };
  handleOpen = () => {
    const open = !this.state.open;
    this.setState({ open });
  };

  render() {
    return (
      <div
        style={{
          // backgroundColor: "#F8F9FA",
          backgroundColor: "#F8F9FA",
          minHeight: "100vh",
          padding: "30px",
        }}
      >
        <ToastContainer />
        <h1>Station List</h1>
        <Modalwindow action="addStation" handleAdd={this.handleAdd} />
        <div>
          <TableContainer
            style={{ backgroundColor: "#F8F9FA" }}
            component={Paper}
          >
            <Table
              // style={{
              //   borderCollapse: "separate",
              //   borderSpacing: "0 10px",
              // }}
              aria-label="simple table"
            >
              <TableHead>
                <TableRow>
                  <StyledCell
                    style={{ fontWeight: 700 }}
                    align="middle"
                  ></StyledCell>
                  <StyledCell align="middle">Station Name</StyledCell>
                  <StyledCell align="middle">ID</StyledCell>
                  <StyledCell align="middle">Lat</StyledCell>
                  <StyledCell align="middle">Long</StyledCell>
                  <StyledCell align="middle">DataList</StyledCell>
                  <StyledCell align="middle">Manage Data</StyledCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.posts.map((post) => (
                  <Ctable
                    key={post._id}
                    post={post}
                    handleUpdate={this.handleUpdate}
                    handleDelete={this.handleDelete}
                    handleDelData={this.handleDelData}
                  />
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
    );
  }
}

export default App;

class Ctable extends Component {
  state = { open: false };
  handleOpen = () => {
    const open = !this.state.open;
    this.setState({ open });
  };
  render() {
    const { post } = this.props;
    return (
      <React.Fragment>
        <TableRow>
          <StyledCell>
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => this.handleOpen()}
            >
              {this.state.open ? (
                <KeyboardArrowUpIcon />
              ) : (
                <KeyboardArrowDownIcon />
              )}
            </IconButton>
          </StyledCell>
          <StyledCell component="th" scope="row">
            {post.name}
          </StyledCell>
          <StyledCell align="middle">{post._id}</StyledCell>
          <StyledCell align="middle">{post.lat}</StyledCell>
          <StyledCell align="middle">{post.long}</StyledCell>
          <StyledCell align="middle">
            {post.fileList.map((file) => (
              <Badges
                key={file.subType}
                mainType={file.mainType}
                subType={file.subType}
              />
            ))}
          </StyledCell>
          <StyledCell align="middle">
            <div className="btn-group">
              <Modalwindow
                action="addData"
                originalName={post.name}
                originalLat={post.lat}
                originalLong={post.long}
                originalfileList={post.fileList}
                originalId={post._id}
                handleUpdate={this.props.handleUpdate}
              />

              <Modalwindow
                action="editStation"
                originalName={post.name}
                originalLat={post.lat}
                originalLong={post.long}
                originalfileList={post.fileList}
                originalId={post._id}
                handleUpdate={this.props.handleUpdate}
              />

              <button
                className="btn btn-danger btn-sm ml-3"
                onClick={() => this.props.handleDelete(post)}
              >
                Delete
              </button>
            </div>
          </StyledCell>
        </TableRow>
        <TableRow>
          <StyledCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={this.state.open} timeout="auto" unmountOnExit>
              <Box margin={1}>
                <Typography variant="h6" gutterBottom component="div">
                  Data List
                </Typography>
                <Table size="small" aria-label="purchases">
                  <TableHead>
                    <TableRow>
                      <StyledCell>mainType</StyledCell>
                      <StyledCell>subType</StyledCell>
                      <StyledCell>Visualize Format</StyledCell>
                      <StyledCell>FileName</StyledCell>
                      <StyledCell>Edit</StyledCell>
                      <StyledCell>Delete</StyledCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {post.fileList.map((file) => (
                      <TableRow key={file.subType}>
                        <StyledCell component="th" scope="row">
                          {file.mainType}
                        </StyledCell>
                        <StyledCell>{file.subType}</StyledCell>
                        <StyledCell>{file.visualFormat}</StyledCell>
                        <StyledCell align="middle">{file.fileName}</StyledCell>
                        <StyledCell align="middle">
                          <Modalwindow
                            action="editData"
                            originalFilename={file.fileName}
                            originalsubType={file.subType}
                            originalmainType={file.mainType}
                            originalvisualFormat={file.visualFormat}
                            originalName={post.name}
                            originalLat={post.lat}
                            originalLong={post.long}
                            originalfileList={post.fileList}
                            originalId={post._id}
                            handleUpdate={this.props.handleUpdate}
                          />
                        </StyledCell>
                        <StyledCell align="middle">
                          <button
                            className="btn btn-danger btn-sm"
                            onClick={() => this.props.handleDelData(post, file)}
                          >
                            Delete
                          </button>
                        </StyledCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </StyledCell>
        </TableRow>
      </React.Fragment>
    );
  }
}
